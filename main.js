//this is my combat game 


// in the prototypal method its good practice to define props on the 
// constructor while defining methods using the Object.protptype
//looks cleaner and avoids
const Character = function (name, health, damage) { 
    this.name = name;
    this.health = health;
    this.damage = damage;

    //another way of defining a function but this way takes more
    this.hit = function(enemy) {
    enemy.health = enemy.health - this.damage
     }
} 

Character.prototype.fly = function () {
    alert("I'm flying!")
}

// Character.prototype.hit = function(enemy) {
//   enemy.health - this.damage
// }

//Hero subclass
const Hero = function (name="Mar", health=100, damage=10) {
    // this line calls the constructor of Character without this 
    Character.call(this, name, health, damage)
}
//this sets the hero prototype equal to an new object that is based on the 
//character prototype
Hero.prototype = Object.create(Character.prototype)

Hero.prototype.element = function(enemy){

    let self = this
    let root = document.getElementById('root');
    let attackButton = document.createElement('button')
    attackButton.innerHTML = "attack"
    
    attackButton.onclick = function(){
        self.hit(enemy)
        console.log(villain.health)
        winCondition()
        healthBar2.style.width = `${villain.health}px`
        healthBar2.style.border = "solid green"
    }
    
   root.appendChild(attackButton)
   const healthBar2 = document.getElementById("healthBar2")
}
// Villain subclass
const Villain = function (name="Ram", health=120, damage=10) {
    Character.call(this, name, health, damage)
}

Villain.protoype = Object.create(Character.prototype)

Villain.prototype.element = function(enemy) {
    let self = this
    let root = document.getElementById('root2');
    const attackButton = document.createElement('button')
    attackButton.innerHTML = "attack"
    
    attackButton.onclick = function(){
        self.hit(enemy)
        console.log(hero.health)
        winCondition()
        healthBar.style.width = `${hero.health}px`
        healthBar.style.border = "solid blue"

    }

   root.appendChild(attackButton)
   const healthBar = document.getElementById("healthBar");
 

   
}

//this function sets up the win condition
const winCondition = function () {
    if(villain.health < 0) {
        alert("Hero Wins!")
        location.reload(true)
    }
    if(hero.health < 0) {
        alert("Villain wins Mwahahaha!")
        location.reload(true)
    }
}
//Hero instance and method call

const hero = new Hero()
const villain = new Villain()

// method call

hero.element(villain)
villain.element(hero)














// const sPunchButton = document.getElementById("sPunch")
// const sKickButton = document.getElementById("sKick")
// const sThrowButton = document.getElementById("sThrow")
